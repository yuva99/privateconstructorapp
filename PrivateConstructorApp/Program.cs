﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrivateConstructorApp
{
    class Program
    {
        static void Main(string[] args)
        {
            PrivateConstructorApp objprivateConstructorApp = new PrivateConstructorApp(10);
           objprivateConstructorApp.display();
            // if you want to initialize or creating instance outside the class then need to have add another  public constructor with parameter 
            //if YOU  dont have  write public constructor with parameter then only accesibe in within the class means we initiliza under the this OR SAME 0 class

        }
    }

    class PrivateConstructorApp
    {
        public int x = 0;
        public   PrivateConstructorApp(int a)
        {
            x = a;
        }

        private PrivateConstructorApp()
        {



        }

       // PrivateConstructorApp objPrivateConstructorApp = new PrivateConstructorApp();

        public void  display()
        {
            Console.WriteLine(x);
            Console.ReadLine();

        }
    }

}
